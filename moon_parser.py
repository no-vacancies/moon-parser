from flask import Flask, render_template, request

import csv
import roman

app = Flask(__name__)



@app.route('/moon-parser/', methods=['GET', 'POST'])
def parse():
    input_value = request.form.get('input', None)
    output_value = ""

    if request.method == 'POST':
        input_value = request.form.get('input')
        try:
            reader = csv.reader(input_value.splitlines(), delimiter='\t')
            current = None
            moon_ores = dict()
            lines = list()
            for row in reader:
                if len(row) == 0:
                    continue
                if len(row) == 1:
                    current = row[0]
                    moon_ores[current] = dict()
                elif current:
                    moon_ores[current][row[1]] = row[2]
            for moon, ores in moon_ores.items():
                line = '{}'.format(moon)
                #line = moon
                for ore, amount in ores.items():
                    line += '\t{}\t{}'.format(ore, amount)
                lines.append(line)
            output_value = '\n'.join(sorted(lines,key=sort_moon))
        except roman.InvalidRomanNumeralError or IndexError:
            output_value = "Couldn't parse input value. Make sure to use a direct paste from the moon scan window."

    return render_template('input.html', input_value=input_value or '', output_value = output_value or '')


@app.errorhandler(404)
def page_not_found(e):
    return "Page not found. Go yell at cath or stop being a bad.", 404


def sort_moon(line: str) -> int:
    moon = line.split('\t')
    tokens = moon[0].split(' ')
    return roman.fromRoman(tokens[1])*100 + int(tokens[-1])


if __name__ == '__main__':
    app.run()
